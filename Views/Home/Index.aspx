﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        <%= Html.Encode(ViewData["Message"]) %></h2>
    <p>
        To learn more about ASP.NET MVC visit <a href="http://asp.net/mvc" title="ASP.NET MVC Website">
            http://asp.net/mvc</a>.
    </p>
    <%using (Html.BeginForm("Validate", "Home", FormMethod.Post, new { id = "LoginForm" }))
      {%>
    <%=Html.TextBox("ValidateCode")%>&nbsp;
    <img id="ImgValidateCode" alt="" src="<%=Url.Action("SecurityCode")%>" />
    <a id="LinkChange">看不清,换一张</a>
    <input type="submit" />
    <%} %>

    <script src="<%=Url.Content("~/Scripts/jquery.form.js") %>" type="text/javascript"></script>

    <script type="text/javascript">


        $(document).ready(function() {

            $("#LoginForm").ajaxForm({
                dataType: "json",
                success: function(data) {
                    alert(data);
                    var timestamp = (new Date()).valueOf();
                    $("#ImgValidateCode").attr("src", '/Home/SecurityCode?time=' + timestamp);
                }
            });

            $("#LinkChange").click(function() {
                var timestamp = (new Date()).valueOf();
                $("#ImgValidateCode").attr("src", '/Home/SecurityCode?time=' + timestamp);
                return false;
            });

        });
        
    
   
    
    </script>

</asp:Content>
